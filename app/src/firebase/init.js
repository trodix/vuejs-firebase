import firebase from 'firebase'
// Initialize Firebase
var config = {
    apiKey: "AIzaSyCHiUo5DdJaEf0RQXAygTjaRLtuPERJeNE",
    authDomain: "fir-upload-files.firebaseapp.com",
    databaseURL: "https://fir-upload-files.firebaseio.com",
    projectId: "fir-upload-files",
    storageBucket: "fir-upload-files.appspot.com",
    messagingSenderId: "955946682868"
  };
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp;