# app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Documentation Firebase

[Firebase Storage Web](https://firebase.google.com/docs/storage/web/)
  * Dealing with Files references on Google Cloud Storage Buckets
  * Upload Files to Google Cloud Storage
  * Download Files to Google Cloud Storage